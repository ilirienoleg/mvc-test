<?php

namespace Core;

class Kernel
{
    /**
     * @var Application
     */
    protected $app;

    /**
     * @var Router
     */
    protected $router;

    /**
     * Kernel constructor.
     * @param Application $app
     * @param Router $router
     */
    public function __construct(Application $app, Router $router)
    {
        $this->app = $app;
        $this->router = $router;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request): Response
    {
        $this->app->instance(Request::class, $request);

        $response = $this->router->dispatch($request);

        if (!isset($response->cookies()['old'])) {
            $response->cookie('old');
        }

        return $response;
    }
}