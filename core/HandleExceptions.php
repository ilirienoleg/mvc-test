<?php

namespace Core;

use Core\Exceptions\HttpException;
use Exception;
use ErrorException;

class HandleExceptions
{
    /**
     * @var Application
     */
    protected $app;

    /**
     * HandleExceptions constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;

        error_reporting(-1);

        set_error_handler([$this, 'handleError']);

        set_exception_handler([$this, 'handleException']);

        register_shutdown_function([$this, 'handleShutdown']);

        ini_set('display_errors', 'Off');
    }

    /**
     * @param $level
     * @param $message
     * @param string $file
     * @param int $line
     * @param array $context
     */
    public function handleError($level, $message, $file = '', $line = 0, $context = []): void
    {
        if (error_reporting() & $level) {
            $this->handleException(new ErrorException($message, 0, $level, $file, $line));
        }
    }

    /**
     * @param Exception $e
     */
    public function handleException($e)
    {
        $view = 'errors/main';
        $code = 500;
        $data = [];

        if ($e instanceof HttpException) {
            $code = $e->getStatusCode();
            $view = 'errors/' . $code;
        } else {
            $data = [
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
            ];
        }

        try {
            $response = new Response(view()->render($view, $data), $code);

            $response->send();
        } catch (Exception $e) {
            //
        }
    }

    /**
     * @return void
     */
    public function handleShutdown(): void
    {
        if (!is_null($error = error_get_last()) && in_array($error['type'], [E_COMPILE_ERROR, E_CORE_ERROR, E_ERROR, E_PARSE])) {
            $this->handleException(new ErrorException($error['message'], $error['type'], 0, $error['file'], $error['line']));
        }
    }
}