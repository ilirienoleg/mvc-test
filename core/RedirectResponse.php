<?php

namespace Core;

class RedirectResponse extends Response
{
    /**
     * @param string $url
     * @param int $statusCode
     */
    public function __construct(string $url, int $statusCode = 302)
    {
        parent::__construct('', $statusCode);

        $this->headers['Location'] = $url;
    }
}