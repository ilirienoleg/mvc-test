<?php

use Core\Application;
use Core\Request;
use Core\Response;
use Core\View;
use Core\RedirectResponse;

/**
 * @param null $abstract
 * @param array $parameters
 * @return Application|mixed
 */
function app($abstract = null, array $parameters = [])
{
    if (is_null($abstract)) {
        return Application::getInstance();
    }

    return Application::getInstance()->make($abstract, $parameters);
}

/**
 * @return Request
 */
function request(): Request
{
    return app(Request::class);
}

/**
 * @return array
 */
function old(): array
{
    return app(Request::class)->old();
}

/**
 * @param string $content
 * @param int $statusCode
 * @return Response
 */
function response(string $content = '', int $statusCode = 200): Response
{
    return new Response($content, $statusCode);
}

/**
 * @param string $url
 * @param int $statusCode
 * @return Response
 */
function redirect(string $url, int $statusCode = 302): Response
{
    return new RedirectResponse($url, $statusCode);
}

/**
 * @param string $template
 * @param array $data
 * @return Response|View
 */
function view(string $template = null, array $data = [])
{
    if (is_null($template)) {
        return app(View::class);
    }

    return app(View::class)->render($template, $data);
}