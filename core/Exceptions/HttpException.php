<?php

namespace Core\Exceptions;

use Throwable;

class HttpException extends \Exception
{
    /**
     * @var
     */
    protected $statusCode;

    /**
     * HttpException constructor.
     * @param $statusCode
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($statusCode, string $message = "", int $code = 0, Throwable $previous = null)
    {
        $this->statusCode = $statusCode;

        parent::__construct($message, $code, $previous);
    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }
}