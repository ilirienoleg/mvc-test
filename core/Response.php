<?php

namespace Core;

class Response
{
    /**
     * @var string
     */
    protected $content;

    /**
     * @var int
     */
    protected $statusCode;

    /**
     * @var array
     */
    protected $headers = [];

    /**
     * @var array
     */
    protected $cookies = [];

    /**
     * @var array
     */
    protected  $statusTexts = [
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',            // RFC2518
        103 => 'Early Hints',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',          // RFC4918
        208 => 'Already Reported',      // RFC5842
        226 => 'IM Used',               // RFC3229
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',    // RFC7238
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Payload Too Large',
        414 => 'URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',                                               // RFC2324
        421 => 'Misdirected Request',                                         // RFC7540
        422 => 'Unprocessable Entity',                                        // RFC4918
        423 => 'Locked',                                                      // RFC4918
        424 => 'Failed Dependency',                                           // RFC4918
        425 => 'Too Early',                                                   // RFC-ietf-httpbis-replay-04
        426 => 'Upgrade Required',                                            // RFC2817
        428 => 'Precondition Required',                                       // RFC6585
        429 => 'Too Many Requests',                                           // RFC6585
        431 => 'Request Header Fields Too Large',                             // RFC6585
        451 => 'Unavailable For Legal Reasons',                               // RFC7725
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates',                                     // RFC2295
        507 => 'Insufficient Storage',                                        // RFC4918
        508 => 'Loop Detected',                                               // RFC5842
        510 => 'Not Extended',                                                // RFC2774
        511 => 'Network Authentication Required',                             // RFC6585
    ];

    /**
     * Response constructor.
     * @param string $content
     * @param int $statusCode
     */
    public function __construct(string $content = '', int $statusCode = 200)
    {
        $this->content = $content;
        $this->statusCode = $statusCode;
    }

    /**
     * @return void
     */
    public function send(): void
    {
        $this->sendHeaders();

        echo $this->content;

        exit;
    }

    /**
     * @param $name
     * @param $value
     * @return Response
     */
    public function header(string $name, string $value): Response
    {
        $this->headers[$name] = $value;

        return $this;
    }

    /**
     * @param array $data
     * @return Response
     */
    public function flash(array $data = []): Response
    {
        $data = base64_encode(json_encode($data));

        $this->cookie('old', $data, time() + 60);

        return $this;
    }

    /**
     * @param string $name
     * @param string $value
     * @param int $expire
     * @param string $path
     * @param string $other
     * @return Response
     */
    public function cookie(string $name, string $value = '', int $expire = 0, string $path = '/', string $other = ''): Response
    {
        $this->cookies[$name] = $this->buildCookie($name, $value, $expire, $path, $other);

        return $this;
    }

    /**
     * @return array
     */
    public function cookies(): array
    {
        return $this->cookies;
    }

    /**
     * @return void
     */
    protected function sendHeaders(): void
    {
        foreach ($this->headers as $name => $header) {
            header($name . ': ' . $header, true, $this->statusCode);
        }

        foreach ($this->cookies as $cookie) {
            header('Set-Cookie:' . $cookie, false, $this->statusCode);
        }

        header('HTTP/1.0 ' . $this->statusCode . ' ' . $this->statusTexts[$this->statusCode], true, $this->statusCode);
    }

    /**
     * @param string $name
     * @param string $value
     * @param int $expire
     * @param string $path
     * @param string $other
     * @return string
     */
    protected function buildCookie(string $name, string $value, int $expire, string $path, string $other): string
    {
        $cookie = $name . '=';

        if ($value === '') {
            $cookie .= 'deleted; expires=' . gmdate('D, d-M-Y H:i:s T', time() - 31536001) . '; Max-Age=0';
        } else {
            $cookie .= $value;

            if ($expire !== 0) {
                $maxAge = $expire - time();

                $cookie .= '; expires=' . gmdate('D, d-M-Y H:i:s T', $expire) . '; Max-Age=' . $maxAge;
            }
        }

        if ($path !== '') {
            $cookie .= '; path=' . $path;
        }

        if ($other !== '') {
            $cookie .= '; ' . $other;
        }

        return $cookie;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->content;
    }
}