<?php

namespace Core;

use LogicException;

class Route
{
    /**
     * @var string
     */
    protected $method;

    /**
     * @var string
     */
    protected $uri;

    /**
     * @var array
     */
    protected $action;

    /**
     * @var array
     */
    protected $where = [];

    /**
     * @var string
     */
    protected $expression;

    /**
     * Route constructor.
     * @param string $method
     * @param string $uri
     * @param $action
     */
    public function __construct(string $method, string $uri, $action)
    {
        $this->method = $method;
        $this->uri = $uri;
        $this->action = $this->parseAction($uri, $action);
    }

    /**
     * @param string $name
     * @param string $expression
     * @return Route
     */
    public function where(string $name, string $expression): Route
    {
        $this->where[$name] = $expression;

        return $this;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function matches(Request $request): bool
    {
        return preg_match($this->compile(), $request->uri());
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function run(Request $request): Response
    {
        preg_match($this->expression, $request->uri(), $matches);

        $variables = $this->resolveVariables($matches);

        if (is_callable($this->action['action'])) {
            $response = $this->action['action']($variables);
        } else {
            $controller = $this->action['controller'];

            $response = call_user_func_array([new $controller(Application::getInstance()), $this->action['action']], $variables);
        }

        return $response instanceof Response
            ? $response
            : new Response($response ?? '');
    }

    /**
     * @param array $matches
     * @return array
     */
    protected function resolveVariables(array $matches): array
    {
        $variables = [];

        foreach ($matches as $key => $match) {
            if (!is_numeric($key)) {
                $variables[$key] = $match;
            }
        }

        return $variables;
    }

    /**
     * @return string
     */
    protected function compile(): string
    {
        $expression = preg_replace_callback('/\{(\w+?)\}/', function ($matches) {
            $expression = $this->where[$matches[1]] ?? '[^/]++';
            return '(?P<' . $matches[1] . '>' . $expression . ')';
        }, $this->uri);

        $this->expression = '#^' . $expression . '$#uDs';

        return $this->expression;
    }

    /**
     * @param $uri
     * @param $action
     * @return array
     * @throws LogicException
     */
    protected function parseAction($uri, $action): array
    {
        if (is_callable($action)) {
            return ['action' => $action];
        } elseif (strpos($action, '@') !== false) {
            list($controller, $action) = explode('@', $action);

            return ['action' => $action, 'controller' => 'App\Controllers\\' . $controller];
        }

        throw new LogicException("You haven't specified action for {$uri}");
    }
}