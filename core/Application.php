<?php

namespace Core;

use Exception;
use ReflectionClass;
use ReflectionParameter;

class Application
{
    /**
     * @var null
     */
    protected static $instance = null;

    /**
     * @var array
     */
    protected $instances = [];

    /**
     * @var array
     */
    protected $with = [];

    /**
     * @var array
     */
    protected $configs = [];

    /**
     * Application constructor.
     */
    public function __construct()
    {
        static::setInstance($this);

        $this->instance(Application::class, $this);

        $this->make(HandleExceptions::class);

        $this->loadConfigs();
    }

    /**
     * @return Response
     */
    public function run(): Response
    {
        $kernel = $this->make(Kernel::class);

        return $kernel->handle(Request::capture());
    }

    /**
     * @param string $abstract
     * @param array $parameters
     * @return mixed
     */
    public function make(string $abstract, array $parameters = [])
    {
        return $this->resolve($abstract, $parameters);
    }

    /**
     * @param string $abstract
     * @param mixed $instance
     * @return mixed
     */
    public function instance(string $abstract, $instance)
    {
        return $this->instances[$abstract] = $instance;
    }

    /**
     * @param string $name
     * @return array
     */
    public function configs(string $name = null): array
    {
        if (is_null($name)) {
            return $this->configs;
        }

        return $this->configs[$name];
    }

    /**
     * @param string $abstract
     * @param array $parameters
     * @return mixed
     * @throws Exception
     */
    protected function resolve(string $abstract, array $parameters = [])
    {
        if (isset($this->instances[$abstract])) {
            return $this->instances[$abstract];
        }

        $this->with[] = $parameters;

        $reflector = new ReflectionClass($abstract);

        if (!$reflector->isInstantiable()) {
            throw new Exception("Can't instantiate {$abstract}");
        }

        $constructor = $reflector->getConstructor();

        if (is_null($constructor)) {
            return new $abstract;
        }

        $dependencies = $constructor->getParameters();
        $instances = $this->resolveDependencies($dependencies);

        if (count($this->with)) {
            array_pop($this->with);
        }

        return $reflector->newInstanceArgs($instances);
    }

    /**
     * @param array $dependencies
     * @return array
     */
    protected function resolveDependencies(array $dependencies): array
    {
        $results = [];

        foreach ($dependencies as $dependency) {
            if (count($this->with) && array_key_exists($dependency->name, end($this->with))) {
                $results[] = end($this->with)[$dependency->name];

                continue;
            }

            $results[] = is_null($dependency->getClass())
                ? $this->resolveDefault($dependency)
                : $this->resolveClass($dependency);
        }

        return $results;
    }

    /**
     * @param ReflectionParameter $parameter
     * @return mixed
     * @throws Exception
     */
    protected function resolveDefault(ReflectionParameter $parameter)
    {
        if ($parameter->isDefaultValueAvailable()) {
            return $parameter->getDefaultValue();
        }

        throw new Exception("Can't resolve parameter {$parameter}");
    }

    /**
     * @param ReflectionParameter $parameter
     * @return mixed
     * @throws Exception
     */
    protected function resolveClass(ReflectionParameter $parameter)
    {
        try {
            return $this->make($parameter->getClass()->name);
        } catch (Exception $e) {
            if ($parameter->isOptional()) {
                return $parameter->getDefaultValue();
            }

            throw $e;
        }
    }

    /**
     * @return void
     */
    protected function loadConfigs(): void
    {
        $configs = scandir($dir = __DIR__ . '/../configs');

        foreach ($configs as $file) {
            if (is_file($dir . '/' . $file)) {
                $this->configs[basename($file, '.php')] = require $dir . '/' . $file;
            }
        }
    }

    /**
     * @return Application
     */
    public static function getInstance(): Application
    {
        if (is_null(static::$instance)) {
            return new static;
        }

        return static::$instance;
    }

    /**
     * @param Application $instance
     * @return Application
     */
    public static function setInstance(Application $instance): Application
    {
        return static::$instance = $instance;
    }
}