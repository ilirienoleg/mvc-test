<?php

namespace Core;

class Request
{
    /**
     * @var array
     */
    protected $query = [];

    /**
     * @var array
     */
    protected $input = [];

    /**
     * @var array
     */
    protected $cookies = [];

    /**
     * @var array
     */
    protected $files = [];

    /**
     * @var array
     */
    protected $server = [];

    /**
     * Request constructor.
     * @param array $query
     * @param array $input
     * @param array $cookies
     * @param array $files
     * @param array $server
     */
    public function __construct(array $query = [], array $input = [], array $cookies = [], array $files = [], array $server = [])
    {
        $this->query = $query;
        $this->input = $input;
        $this->cookies = $cookies;
        $this->files = $files;
        $this->server = $server;
    }

    /**
     * @return Request
     */
    public static function capture(): Request
    {
        return new static($_GET, $_POST, $_COOKIE, $_FILES, $_SERVER);
    }

    /**
     * @param null $key
     * @param null $default
     * @return array|string
     */
    public function query($key = null, $default = null)
    {
        return $this->getByType(__FUNCTION__, $key, $default);
    }

    /**
     * @param null $key
     * @param null $default
     * @return array|string
     */
    public function input($key = null, $default = null)
    {
        return $this->getByType(__FUNCTION__, $key, $default);
    }

    /**
     * @param null $key
     * @param null $default
     * @return array|string
     */
    public function cookies($key = null, $default = null)
    {
        return $this->getByType(__FUNCTION__, $key, $default);
    }

    /**
     * @param null $key
     * @param null $default
     * @return array|string
     */
    public function files($key = null, $default = null)
    {
        return $this->getByType(__FUNCTION__, $key, $default);
    }

    /**
     * @param null $key
     * @param null $default
     * @return array|string
     */
    public function server($key = null, $default = null)
    {
        return $this->getByType(__FUNCTION__, $key, $default);
    }

    /**
     * @param $type
     * @param null $key
     * @param null $default
     * @return array|string
     */
    protected function getByType($type, $key = null, $default = null)
    {
        if (!is_null($key)) {
            return $this->{$type}[$key] ?? $default;
        }

        return $this->{$type};
    }

    /**
     * @return string
     */
    public function uri(): string
    {
        $uri = $this->server['REQUEST_URI'];

        if ( false !== $pos = strpos($uri, '?')) {
            $uri = substr($uri, 0, $pos);
        }

        return $uri;
    }

    /**
     * @param array $data
     * @return string
     */
    public function queryString(array $data = []): string
    {
       $data = array_merge($this->query, $data);

        return '?' . http_build_query($data);
    }

    /**
     * @return string
     */
    public function method(): string
    {
        return strtolower($this->server['REQUEST_METHOD']);
    }

    /**
     * @return array
     */
    public function old(): array
    {
        if( !isset($this->cookies['old'])) {
            return [];
        }

        return json_decode(base64_decode($this->cookies['old']), true);
    }
}