<?php

namespace Core;


use \Exception;
use \InvalidArgumentException;

class View
{
    /**
     * @var Application
     */
    protected $app;

    /**
     * @var
     */
    protected $path = __DIR__ . '/../app/Views/';

    /**
     * View constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @param string $template
     * @param array $data
     * @return Response
     * @throws InvalidArgumentException
     */
    public function render(string $template, array $data = []): Response
    {
        if (file_exists($view = $this->path . $template . '.php')) {
            return new Response($this->getContent($view, $data));
        }

        throw new InvalidArgumentException("Can't find {$template} template");
    }

    /**
     * @param string $view
     * @param $data
     * @return mixed
     */
    protected function getContent(string $view, $data)
    {
        $obLevel = ob_get_level();

        ob_start();

        extract($data, EXTR_SKIP);

        try {
            include $view;
        } catch (Exception $e) {
           $this->handleException($e, $obLevel);
        }

        return ob_get_clean();
    }

    /**
     * @param Exception $e
     * @param $obLevel
     * @throws Exception
     */
    protected function handleException(Exception $e, $obLevel)
    {
        while (ob_get_level() > $obLevel) {
            ob_end_clean();
        }

        throw $e;
    }
}