<?php

namespace Core\Database;

class Model
{
    /**
     * @var string
     */
    protected $table;

    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * Model constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->attributes = $attributes;
    }

    /**
     * @param array $attributes
     * @return bool
     */
    public function create(array $attributes): bool
    {
        $this->fill($attributes);

        $id = $this->newBuilder()->insertGetId($this->attributes);

        $this->fill([
            'id' => $id,
            'created_at' => (new \DateTime())->format('Y-m-d H:i:s'),
            'updated_at' => (new \DateTime())->format('Y-m-d H:i:s'),
        ]);

        return true;
    }

    /**
     * @param array $attributes
     * @return bool
     */
    public function update(array $attributes): bool
    {
        if( !isset($this->attributes['id'])) {
            return false;
        }

        $this->fill($attributes + ['updated_at' => (new \DateTime())->format('Y-m-d H:i:s')]);

        return $this->newBuilder()->where('id', '=', $this->attributes['id'])->update($this->attributes);
    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        if( !isset($this->attributes['id'])) {
            return false;
        }

        return $this->newBuilder()->where('id', '=', $this->attributes['id'])->delete();
    }

    /**
     * @param array $attributes
     * @return Model
     */
    public function fill(array $attributes): Model
    {
        foreach ($attributes as $key => $attribute) {
            $this->attributes[$key] = $attribute;
        }

        return $this;
    }

    /**
     * @param array $attributes
     * @return Model
     */
    public function newInstance(array $attributes = []): Model
    {
        return new static($attributes);
    }

    /**
     * @return ActiveRecordBuilder
     */
    public function newBuilder(): ActiveRecordBuilder
    {
        return (new ActiveRecordBuilder($this->newBaseBuilder()))
            ->setModel($this);
    }

    /**
     * @return Builder
     */
    public function newBaseBuilder(): Builder
    {
        return (new Builder())->table($this->table);
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        if (isset($this->attributes[$name])) {
            return $this->attributes[$name];
        }

        return null;
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return $this->newBuilder()->{$name}(...$arguments);
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public static function __callStatic($name, $arguments)
    {
        return (new static())->{$name}(...$arguments);
    }
}