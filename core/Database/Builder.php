<?php

namespace Core\Database;

use Core\Request;
use PDO;

class Builder
{
    /**
     * @var PDO
     */
    protected $connection;

    /**
     * @var string
     */
    protected $table;

    /**
     * @var array
     */
    protected $bindings = [];

    /**
     * @var array
     */
    protected $wheres = [];

    /**
     * @var array
     */
    protected $groups = [];

    /**
     * @var array
     */
    protected $orders = [];

    /**
     * @var int
     */
    protected $limit;

    /**
     * @var int
     */
    protected $offset;

    /**
     * @var array
     */
    protected $columns;

    /**
     * Builder constructor.
     */
    public function __construct()
    {
        $this->connection = app(Connector::class)->connection();
    }

    /**
     * @param array $columns
     * @return array
     */
    public function get($columns = ['*']): array
    {
        $this->columns = $columns;

        $sql = $this->compileSelect();

        $query = $this->connection->prepare($sql);

        $query->execute($this->bindings);

        return $query->fetchAll();
    }

    /**
     * @param array $values
     * @return bool
     */
    public function insert(array $values): bool
    {
        $sql = $this->compileInsert($values);

        $query = $this->connection->prepare($sql);

        return $query->execute(array_values($values));
    }

    /**
     * @param array $values
     * @return int
     */
    public function insertGetId(array $values): int
    {
        $sql = $this->compileInsert($values);

        $query = $this->connection->prepare($sql);

        $query->execute(array_values($values));

        return $this->connection->lastInsertId();
    }

    /**
     * @param array $values
     * @return bool
     */
    public function update(array $values): bool
    {
        $sql = $this->compileUpdate($values);

        $query = $this->connection->prepare($sql);

        $bindings = array_merge(array_values($values), $this->bindings);

        return $query->execute($bindings);
    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        $sql = $this->compileDelete();

        $query = $this->connection->prepare($sql);

        return $query->execute($this->bindings);
    }

    /**
     * @param int $perPage
     * @param array $columns
     * @return array
     */
    public function paginate($perPage = 15, $columns = ['*']): array
    {
        $page = app(Request::class)->query('page', 1);

        $total = $this->getCountForPagination();

        $results = $total ? $this->forPage($page, $perPage)->get($columns) : [];

        return [
            'data' => $results,
            'current_page' => $page,
            'total' => $total,
            'last' => max((int)ceil($total / $perPage), 1),
            'per_page' => $perPage
        ];
    }

    /**
     * @param $page
     * @param int $perPage
     * @return Builder
     */
    public function forPage(int $page, int $perPage = 15): Builder
    {
        return $this->offset(($page - 1) * $perPage)->limit($perPage);
    }

    /**
     * @return int
     */
    public function getCountForPagination(): int
    {
        $result = $this->get(['count(*) as count']);

        if (!isset($result[0])) {
            return 0;
        }

        return $result[0]['count'];
    }

    /**
     * @param string $table
     * @return Builder
     */
    public function table(string $table): Builder
    {
        $this->table = $table;

        return $this;
    }

    /**
     * @param string $column
     * @param string $operator
     * @param string $value
     * @param string $boolean
     * @return Builder
     */
    public function where(string $column, string $operator, string $value, string $boolean = 'and'): Builder
    {
        $where = compact('column', 'operator', 'boolean');

        $this->wheres[] = $where;
        $this->bindings[] = $value;

        return $this;
    }

    /**
     * @param array $groups
     * @return Builder
     */
    public function groupBy(...$groups): Builder
    {
        $this->groups = array_merge($groups, $this->groups);

        return $this;
    }

    /**
     * @param string $column
     * @param string $direction
     * @return Builder
     */
    public function orderBy(string $column, string $direction = 'asc'): Builder
    {
        $orders = compact('column', 'direction');
        $this->orders[] = $orders;

        return $this;
    }

    /**
     * @param int $value
     * @return Builder
     */
    public function limit(int $value): Builder
    {
        $this->limit = $value;

        return $this;
    }

    /**
     * @param int $value
     * @return Builder
     */
    public function offset(int $value): Builder
    {
        $this->offset = $value;

        return $this;
    }

    /**
     * @return string
     */
    protected function compileSelect(): string
    {
        $columns = $this->compileColumns();
        $wheres = $this->compileWheres();
        $groups = $this->compileGroups();
        $orders = $this->compileOrders();
        $limit = $this->compileLimit();
        $offset = $this->compileOffset();
        $table = $this->table;

        return "{$columns} from {$table}{$wheres}{$groups}{$orders}{$limit}{$offset}";
    }

    /**
     * @param array $values
     * @return string
     */
    protected function compileInsert(array $values): string
    {
        $table = $this->table;
        $columns = $this->columnize(array_keys($values));
        $parameters = '(' . $this->parametrize($values) . ')';

        return "insert into $table ($columns) values $parameters";
    }

    /**
     * @param array $values
     * @return string
     */
    protected function compileUpdate(array $values): string
    {
        $table = $this->table;
        $wheres = $this->compileWheres();
        $columns = implode(', ', array_map(function ($value, $key) {
            return $key . ' = ?';
        }, $values, array_keys($values)));

        return "update {$table} set $columns$wheres";
    }

    /**
     * @return string
     */
    protected function compileDelete(): string
    {
        $table = $this->table;
        $wheres = $this->compileWheres();

        return "delete from $table$wheres";
    }

    /**
     * @return string
     */
    protected function compileColumns(): string
    {
        return 'select ' . $this->columnize($this->columns);
    }

    /**
     * @return string
     */
    protected function compileWheres(): string
    {
        if (count($this->wheres) == 0) {
            return '';
        }

        $wheres = array_map(function ($where) {
            return $where['boolean'] . ' ' . $where['column'] . ' ' . $where['operator'] . ' ?';
        }, $this->wheres);

        return ' where ' . preg_replace('/and |or /i', '', implode(' ', $wheres), 1);
    }

    /**
     * @return string
     */
    protected function compileGroups(): string
    {
        if (count($this->groups) == 0) {
            return '';
        }

        return ' group by ' . $this->columnize($this->groups);
    }

    /**
     * @return string
     */
    protected function compileOrders(): string
    {
        if (count($this->orders) == 0) {
            return '';
        }

        $orders = array_map(function ($order) {
            return $order['column'] . ' ' . $order['direction'];
        }, $this->orders);

        return ' order by ' . implode(', ', $orders);
    }

    /**
     * @return string
     */
    protected function compileLimit(): string
    {
        if (is_null($this->limit)) {
            return '';
        }

        return ' limit ' . $this->limit;
    }

    /**
     * @return string
     */
    protected function compileOffset(): string
    {
        if (is_null($this->offset)) {
            return '';
        }

        return ' offset ' . $this->offset;
    }

    /**
     * @param array $columns
     * @return string
     */
    protected function columnize(array $columns): string
    {
        return implode(', ', $columns);
    }

    /**
     * @param array $parameters
     * @return string
     */
    protected function parametrize(array $parameters): string
    {
        return implode(', ', array_map(function ($parametr) {
            return '?';
        }, $parameters));
    }
}