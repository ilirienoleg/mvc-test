<?php

namespace Core\Database;

use Core\Application;
use PDO;

class Connector
{
    /**
     * @var null
     */
    protected $connection = null;

    /**
     * @var Application
     */
    protected $app;

    /**
     * DatabaseConnector constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @return PDO
     */
    public function connection(): PDO
    {
        if (is_null($this->connection)) {
            return $this->createConnection($this->app->configs('database'));
        }

        return $this->connection;
    }

    /**
     * @param array $config
     * @return PDO
     */
    protected function createConnection(array $config): PDO
    {
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES {$config['charset']}",
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        ];

        return $this->connection = new PDO($this->getDsn($config), $config['username'], $config['password'], $options);
    }

    /**
     * @param array $config
     * @return string
     */
    protected function getDsn(array $config): string
    {
        return "mysql:host={$config['host']};port={$config['port']};dbname={$config['database']};charset={$config['charset']}";
    }
}