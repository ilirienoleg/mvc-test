<?php

namespace Core\Database;

use Core\Request;

class ActiveRecordBuilder
{
    /**
     * @var Builder
     */
    protected $builder;

    /**
     * @var Model
     */
    protected $model;

    /**
     * ActiveRecordBuilder constructor.
     * @param Builder $builder
     */
    public function __construct(Builder $builder)
    {
        $this->builder = $builder;
    }

    /**
     * @param array $columns
     * @return Model[]
     */
    public function get($columns = ['*'])
    {
        $models = $this->builder->get($columns);

        $return = [];

        foreach ($models as $model) {
            $return[] = $this->model->newInstance($model);
        }

        return $return;
    }

    /**
     * @param array $columns
     * @return Model|null
     */
    public function first($columns = ['*'])
    {
        $models = $this->builder->limit(1)->get($columns);

        if( count($models) == 0 ) {
            return null;
        }

        return $this->model->newInstance(array_shift($models));
    }

    /**
     * @param int $perPage
     * @param array $columns
     * @return array
     */
    public function paginate($perPage = 15, $columns = ['*']): array
    {
        $page = app(Request::class)->query('page', 1);
        $page = $page > 0 ? $page : 1;

        $total = $this->builder->getCountForPagination();

        $results = $total ? $this->forPage($page, $perPage)->get($columns) : [];

        return [
            'data' => $results,
            'current_page' => $page,
            'total' => $total,
            'last' => max((int)ceil($total / $perPage), 1),
            'per_page' => $perPage,
        ];
    }

    /**
     * @param Model $model
     * @return ActiveRecordBuilder
     */
    public function setModel(Model $model): ActiveRecordBuilder
    {
        $this->model = $model;

        return $this;
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        if (in_array($name, ['insert', 'update', 'insertGetId', 'delete'])) {
            return $this->builder->{$name}(...$arguments);
        }

        $this->builder->{$name}(...$arguments);

        return $this;
    }
}