<?php

namespace Core;

use Core\Exceptions\HttpException;

class Router
{
    /**
     * @var Router
     */
    protected static $instance = null;

    /**
     * @var Route[]
     */
    protected $allRoutes = [];

    /**
     * @var array
     */
    protected $routes = [];

    /**
     * @var Application
     */
    protected $app;

    /**
     * Router constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        static::setInstance($this);

        $this->app = $app;
        $this->routes = [];
    }

    /**
     * @param $method string
     * @param $uri string
     * @param $controller mixed
     * @return Route
     */
    protected function addRoute(string $method, string $uri, $controller): Route
    {
        $route = new Route($method, $uri, $controller);

        $this->allRoutes[] = $route;
        $this->routes[$method][] = $route;

        return $route;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function dispatch(Request $request): Response
    {
        $this->mapRoutes();

        return $this->runRoute($request, $this->findRoute($request));
    }

    /**
     * @param Request $request
     * @param Route $route
     * @return Response|null
     */
    protected function runRoute(Request $request, Route $route): Response
    {
        return $route->run($request);
    }

    /**
     * @param Request $request
     * @return Route
     * @throws HttpException
     */
    protected function findRoute(Request $request): ?Route
    {
        foreach ($this->routes($request->method()) as $route) {
            if ($route->matches($request)) {
                return $route;
            }
        }

        throw new HttpException(404);
    }

    /**
     * @return void
     */
    protected function mapRoutes(): void
    {
        require __DIR__ . '/../routes/routes.php';
    }

    /**
     * @param null|string $method
     * @return Route[]
     */
    protected function routes(?string $method = null): array
    {
        if (is_null($method)) {
            return $this->allRoutes;
        }

        return $this->routes[$method] ?? [];
    }

    /**
     * @param Router $instance
     * @return Router
     */
    public static function setInstance(Router $instance): Router
    {
        return static::$instance = $instance;
    }

    /**
     * @param $uri string
     * @param $controller mixed
     * @return Route
     */
    public static function get(string $uri, $controller): Route
    {
        return static::$instance->addRoute('get', $uri, $controller);
    }

    /**
     * @param $uri string
     * @param $controller mixed
     * @return Route
     */
    public static function post(string $uri, $controller): Route
    {
        return static::$instance->addRoute('post', $uri, $controller);
    }

    /**
     * @param $uri string
     * @param $controller mixed
     * @return Route
     */
    public static function put(string $uri, $controller): Route
    {
        return static::$instance->addRoute('put', $uri, $controller);
    }

    /**
     * @param $uri string
     * @param $controller mixed
     * @return Route
     */
    public static function delete(string $uri, $controller): Route
    {
        return static::$instance->addRoute('delete', $uri, $controller);
    }
}