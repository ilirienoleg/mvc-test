<?php

require __DIR__ . '/../vendor/autoload.php';

$app = new \Core\Application();

$response = $app->run();

$response->send();