<?php

use Core\Router;

Router::get('/', 'HomeController@index');
Router::post('/tasks', 'TasksController@store');
Router::get('/tasks/create', 'TasksController@create');
Router::get('/tasks/{task}/edit', 'TasksController@edit')->where('task', '[0-9]+');
Router::post('/tasks/{task}', 'TasksController@update')->where('task', '[0-9]+');
Router::get('/admin/login', 'AdminController@showLoginForm');
Router::post('/admin/login', 'AdminController@login');
