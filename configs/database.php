<?php

return [
    'host' => 'localhost',
    'port' => '3306',
    'database' => 'beejee',
    'username' => 'root',
    'password' => 'secret',
    'charset' => 'utf8',
];