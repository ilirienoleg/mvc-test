<?php

namespace App\Controllers;

use Core\Response;
use Rakit\Validation\Validation;
use Rakit\Validation\Validator;

abstract class Controller
{
    /**
     * @param array $request
     * @param array $rules
     * @return Validation
     */
    protected function validate(array $request, array $rules): Validation
    {
        $validator = new Validator();

        $validation = $validator->make($request, $rules);

        $validation->validate();

        return $validation;
    }

    /**
     * @param string $url
     * @param string $message
     * @return Response
     */
    protected function storedSuccessfullyResponse(string $url, string $message): Response
    {
        return redirect($url)
            ->flash(['success' => $message]);
    }

    /**
     * @param string $url
     * @param array $request
     * @param Validation|array $errors
     * @return Response
     */
    protected function validationFailedResponse(string $url, array $request, $errors): Response
    {
        return redirect($url)
            ->flash([
                'input' => $request,
                'errors' => $errors instanceof Validation ? $errors->errors()->all() : $errors,
            ]);
    }
}