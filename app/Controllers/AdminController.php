<?php

namespace App\Controllers;

use App\Auth;
use App\Models\User;

class AdminController extends Controller
{
    /**
     * @return \Core\Response
     */
    public function showLoginForm()
    {
        if( Auth::logged()) {
            return redirect('/');
        }

        return view('admin/login');
    }

    /**
     * @return \Core\Response
     */
    public function login()
    {
        if( Auth::logged()) {
            return redirect('/');
        }

        $validation = $this->validate($request = request()->input(), [
            'username' => 'required',
            'password' => 'required',
        ]);

        if ($validation->passes()) {
            $hash = hash_hmac('sha256', $request['password'], app()->configs('app')['secret']);

            $user = User::where('username', '=', $request['username'])
                ->where('password', '=', $hash)
                ->first();

            if (!$user) {
                $errors = ['Can\'t find user with this credentials in our database'];

                return $this->validationFailedResponse('/admin/login', $request, $errors);
            }

            return redirect('/')
                ->cookie('auth', Auth::generateHash($user), time() + 60 * 60 * 7);
        }

        return $this->validationFailedResponse('/admin/login', $request, $validation);
    }
}