<?php

namespace App\Controllers;

use App\Models\Task;

class HomeController extends Controller
{
    /**
     * @return \Core\Response
     */
    public function index()
    {
        $validation = $this->validate(request()->query(), [
            'field' => 'in:id,email,name,done',
            'direction' => 'in:asc,desc',
        ]);

        $field = 'id';
        $direction = 'desc';

        if ($validation->passes()) {
            $field = request()->query('field', 'id');
            $direction = request()->query('direction', 'desc');
        }

        $tasks = Task::orderBy($field, $direction)->paginate(3);

        return view('index', compact('tasks', 'direction'));
    }
}