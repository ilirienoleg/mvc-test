<?php

namespace App\Controllers;

use App\Auth;
use App\Models\Task;
use Core\Exceptions\HttpException;

class TasksController extends Controller
{
    /**
     * @return \Core\Response
     */
    public function create()
    {
        return view('tasks/create');
    }

    /**
     * @return \Core\Response
     */
    public function store()
    {
        $validation = $this->validate($request = request()->input(), [
            'name' => 'required',
            'email' => 'required|email',
            'task' => 'required|min:20',
        ]);

        if ($validation->passes()) {
            (new Task())->create($this->payload($request));

            return $this->storedSuccessfullyResponse('/', 'Task successfully added');
        }

        return $this->validationFailedResponse('/tasks/create', $request, $validation);
    }

    /**
     * @param $id
     * @return \Core\Response
     * @throws HttpException
     */
    public function edit($id)
    {
        if( !Auth::logged()) {
            return redirect('/');
        }

        $task = $this->firstOrFail($id);

        return view('tasks/edit', compact('task'));
    }

    /**
     * @param $id
     * @return \Core\Response
     * @throws HttpException
     */
    public function update($id)
    {
        if( !Auth::logged()) {
            return redirect('/');
        }

        $task = $this->firstOrFail($id);

        $validation = $this->validate($request = request()->input(), [
            'name' => 'required',
            'email' => 'required|email',
            'task' => 'required|min:20',
        ]);

        if ($validation->passes()) {
            $task->update($this->payload($request));

            return $this->storedSuccessfullyResponse('/', 'Task successfully edited');
        }

        return $this->validationFailedResponse('/tasks/' . $task->id . '/edit', $request, $validation);
    }

    /**
     * @param $id
     * @return Task
     * @throws HttpException
     */
    protected function firstOrFail($id)
    {
        $task = Task::where('id', '=', $id)->first();

        if (is_null($task)) {
            throw new HttpException(404);
        }

        return $task;
    }

    /**
     * @param $request
     * @return array
     */
    protected function payload($request): array
    {
        return [
            'name' => $request['name'],
            'email' => $request['email'],
            'text' => $request['task'],
            'done' => isset($request['done']) ? 1 : 0,
        ];
    }
}