<ul class="pagination justify-content-center">
    <li class="page-item<?= $pagination['current_page'] == 1 ? ' disabled' : '' ?>"">
        <a class="page-link"
           href="<?= request()->uri() . request()->queryString(['page' => $pagination['current_page'] - 1])?>"
           aria-label="Previous"
        >
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Previous</span>
        </a>
    </li>
    <?php for ($page = 1; $page <= $pagination['last']; $page++) { ?>
        <li class="page-item<?= $pagination['current_page'] == $page ? ' active' : '' ?>">
            <a class="page-link"
               href="<?= request()->uri() . request()->queryString(['page' => $page])?>"
            >
                <?= $page ?>
            </a>
        </li>
    <?php } ?>
    <li class="page-item<?= $pagination['current_page'] == $pagination['last'] ? ' disabled' : '' ?>">
        <a class="page-link"
           href="<?= request()->uri() . request()->queryString(['page' => $pagination['current_page'] + 1])?>"
           aria-label="Next"
        >
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">Next</span>
        </a>
    </li>
</ul>