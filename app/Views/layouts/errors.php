<?php if (isset(old()['errors'])) { ?>
    <div class="alert alert-danger" role="alert">
        <ul class="mb-0">
            <?php foreach (old()['errors'] as $error) { ?>
                <li><?= $error ?></li>
            <?php } ?>
        </ul>
    </div>
<?php } ?>