<?= view()->render('layouts/header') ?>
<?= view()->render('layouts/nav') ?>
    <div class="container mt-4">
        <div class="row">
            <div class="col-12">
                <?= view()->render('layouts/errors') ?>
                <form method="post" action="/tasks/<?= $task->id ?>">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name"
                                   value="<?= $task->name ?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email"
                                   value="<?= $task->email ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="task">Task</label>
                        <textarea class="form-control" name="task" id="task"
                                  placeholder="Task text"><?= $task->text ?></textarea>
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="done" name="done"<?= $task->done ? ' checked' : ''?>>
                        <label class="form-check-label" for="done">Done</label>
                    </div>
                    <button type="submit" class="btn btn-primary">Update task</button>
                </form>
            </div>
        </div>
    </div>
<?= view()->render('layouts/footer') ?>