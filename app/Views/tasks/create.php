<?= view()->render('layouts/header') ?>
<?= view()->render('layouts/nav') ?>
    <div class="container mt-4">
        <div class="row">
            <div class="col-12">
                <?= view()->render('layouts/errors') ?>
                <form method="post" action="/tasks">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name"
                                   value="<?= old()['input']['name'] ?? '' ?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email"
                                   value="<?= old()['input']['email'] ?? '' ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="task">Task</label>
                        <textarea class="form-control" name="task" id="task"
                                  placeholder="Task text"><?= old()['input']['task'] ?? '' ?></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Add task</button>
                </form>
            </div>
        </div>
    </div>
<?= view()->render('layouts/footer') ?>