<?= view()->render('layouts/header') ?>
<?= view()->render('layouts/nav') ?>
<div class="container mt-4">
    <div class="row justify-content-end mb-4">
        <div class="col">
            <?= view()->render('layouts/errors') ?>
            <form method="post" action="/admin/login">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username" placeholder="Enter username" value="<?= old()['input']['username'] ?? '' ?>">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
<?= view()->render('layouts/footer') ?>
