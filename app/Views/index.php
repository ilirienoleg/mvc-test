<?= view()->render('layouts/header') ?>
<?= view()->render('layouts/nav') ?>
<div class="container mt-4">
    <div class="row justify-content-end mb-4">
        <div class="col">
            <?= view()->render('layouts/success') ?>
            <a href="/tasks/create" class="btn btn-success">Add task</a>
        </div>
    </div>
    <div class="row">
        <table class="table">
            <thead>
            <tr>
                <th scope="col"><a href="?field=id&direction=<?= $direction == 'asc' ? 'desc' : 'asc' ?>">#</a></th>
                <th scope="col"><a href="?field=name&direction=<?= $direction == 'asc' ? 'desc' : 'asc' ?>">Name</a></th>
                <th scope="col"><a href="?field=email&direction=<?= $direction == 'asc' ? 'desc' : 'asc' ?>">Email</a></th>
                <th scope="col">Task</th>
                <th scope="col"><a href="?field=done&direction=<?= $direction == 'asc' ? 'desc' : 'asc' ?>">Status</a></th>
                <th scope="col">Create date</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($tasks['data'] as $task) { ?>
                <tr>
                    <?php if (!\App\Auth::logged()) { ?>
                        <th scope="row"><?= $task->id ?></th>
                    <?php } else { ?>
                        <th scope="row"><a href="/tasks/<?= $task->id ?>/edit"><?= $task->id ?></a></th>
                    <?php } ?>
                    <td><?= $task->name ?></td>
                    <td><?= $task->email ?></td>
                    <td><?= $task->text ?></td>
                    <td><?= $task->done ?></td>
                    <td><?= $task->created_at ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col">
            <?= view()->render('layouts/pagination', ['pagination' => $tasks]) ?>
        </div>
    </div>
</div>
<?= view()->render('layouts/footer') ?>
