<?= view()->render('layouts/header') ?>
<div class="container mt-4">
    <div class="row justify-content-end mb-4">
        <div class="col">
            <h1>404 Not Found</h1>
        </div>
    </div>
</div>
<?= view()->render('layouts/footer') ?>
