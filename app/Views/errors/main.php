<?= view()->render('layouts/header') ?>
<div class="container mt-4">
    <div class="row justify-content-end mb-4">
        <div class="col">
            <h1>Error</h1>
            <div class="mb-4"><?= $message ?></div>
            <pre><?= $trace ?></pre>
        </div>
    </div>
</div>
<?= view()->render('layouts/footer') ?>
