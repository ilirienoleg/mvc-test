<?php

namespace App\Models;

use Core\Database\Model;

class Task extends Model
{
    /**
     * @var string
     */
    protected $table = 'tasks';
}