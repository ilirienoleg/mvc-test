<?php

namespace App\Models;

use Core\Database\Model;

class User extends Model
{
    /**
     * @var string
     */
    protected $table = 'users';
}