<?php

namespace App;

use App\Models\User;

class Auth
{
    /**
     * @var null
     */
    protected static $logged = null;

    /**
     * @return bool
     */
    public static function logged(): bool
    {
        if (!is_null(static::$logged)) {
            return static::$logged;
        }

        $cookie = request()->cookies('auth');

        if (is_null($cookie)) {
            return static::$logged = false;
        }

        $decoded = explode(':', base64_decode($cookie));

        if (count($decoded) != 2) {
            return static::$logged = false;
        }

        list($username, $hash) = $decoded;

        $user = User::where('username', '=', $username)->first();

        if (!$user) {
            return static::$logged = false;
        }

        if ($hash !== hash_hmac('sha256', $user->password, app()->configs('app')['secret'])) {
            return static::$logged = false;
        }

        return static::$logged = true;
    }

    /**
     * @param User $user
     * @return string
     */
    public static function generateHash(User $user): string
    {
        return base64_encode($user->username . ':' . hash_hmac('sha256', $user->password, app()->configs('app')['secret']));
    }
}